'use strict';

let canvas = this.__canvas = new fabric.Canvas('vision', {
    width: 1200,
    height: 400
});

canvas.backgroundColor = "rgba(224,224,224,1)";

let insertText = new Vue({
    el: '#insertText',
    data: {
        textToRender: 'Hello, world'
    },
    methods: {
        insert: function() {
            let text = new fabric.Text(this.textToRender, {
                fontFamily: 'Times New Roman',
                fontSize: 20
            });
            canvas.add(text);
        }
    }
});

let insertImage = new Vue({
    el: '#insertImage',
    data: {
        imageURL: 'img/dog.jpeg'
    },
    methods: {
        insert: function() {
            fabric.Image.fromURL(this.imageURL, function(img) {
                canvas.add(img);
            });
        }
    }
});

canvas.renderAll();
